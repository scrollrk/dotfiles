#
# ~/.bashrc
#

### exports ###

export EDITOR='nvim'
export VISUAL='emacs'
export BROWSER='brave'
export TERM='xterm-256color'
export MANPAGER="sh -c 'col -bx | bat -l man -p'" # Use bat for man pager

# if there is an x display use brave else use links
[ -n "$DISPLAY" ] && export BROWSER='brave' || export BROWSER='links'

### path ###

# add emacs, cabal and local to path
export PATH=$HOME/.emacs.d/bin:$HOME/.cabal/bin:$HOME/.local/bin:$PATH

### secrets ###

# enable gnome keyring in terminal
[ -n "$DESKTOP_SESSION" ] && eval $(gnome-keyring-daemon --start) || export SSH_AUTH_SOCK

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# load alias from aliasrc file
[ -f "$HOME/.config/aliasrc" ] && source "$HOME/.config/aliasrc"

PS1='[\u@\h \W]\$ '

