" -------------------------------------------------------------------------
"Auto Commands
" -------------------------------------------------------------------------

" Automatically cd into the directory that the file is in
" autocmd BufEnter * execute "chdir ".escape(expand("%:p:h"), ' ')

" Remove any trailing whitespace that is in the file
autocmd BufRead,BufWrite * if ! &bin | silent! %s/\s\+$//ge | endif

" -------------------------------------------------------------------------
" Plugins
" -------------------------------------------------------------------------

" Plugin Manager
" use :PlugInstall to install plugins
call plug#begin('~/.vim/plugged')

" Syntax Highlighers
Plug 'dag/vim-fish'
Plug 'neovimhaskell/haskell-vim'
Plug 'vim-scripts/rtorrent-syntax-file'
Plug 'rrethy/vim-hexokinase', { 'do': 'make hexokinase' }
Plug 'mboughaba/i3config.vim'
" Color Themes
Plug 'dguo/blood-moon', {'rtp': 'applications/vim'}
Plug 'chriskempson/base16-vim'
" Airline
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'

" Add plugins to &runtimepath
call plug#end()

" -------------------------------------------------------------------------
" Plugin Settings
" -------------------------------------------------------------------------

" hexokinase settings
" fill color word backgrounds
let g:Hexokinase_highlighters = [
"\   'virtual',
"\   'sign_column',
"\   'background',
\   'backgroundfull'
"\   'foreground',
"\   'foregroundfull'
\ ]

" airline settings
let g:airline#extensions#tabline#enabled = 1 " enables airline style tab bar
let g:airline#extensions#tabline#left_sep = ' '
let g:airline#extensions#tabline#left_alt_sep = '|'
let g:airline_skip_empty_sections = 1 " prevents showing sections when empty
let g:airline_powerline_fonts = 1 " enables powerline glyphs
let g:airline_theme='base16_tomorrow_night'

" -------------------------------------------------------------------------
" VIM Settings
" -------------------------------------------------------------------------

" line numbers
set number " show line numbers
set relativenumber " make line numbers relative

" formating
set autoindent " keep indent from preceeding line
set expandtab " replace tabs with spaces CTRL-V<Tab> to insert real tab
set smarttab " tab or <BS> inserts or deletes shiftwidth at beginning of line
set shiftwidth=2 " width of tabs
syntax on " enable syntax highlighting

" search
set hlsearch " hlsearched words :nohl to turn off
set incsearch " enable incremental search
set ignorecase " include uppercase words when searching lowercase
set smartcase " include only uppercase words when searching uppercase

" misc
set encoding=utf-8 " set internal encoding of vim
set nocompatible " set compatbility to vim only
set hidden " hide buffers when they are abandoned
set backspace=indent,eol,start " fixes common backstpace problems
set termguicolors " enable 24-bit RGB color in TUI
"set mouse=a " enable mouse usage (all modes)

" don't seem to need these settings with airline
"set showmode " show mode in status line
"set showcmd " show (partial) command in status line

" colorscheme
colorscheme base16-tomorrow-night

" -------------------------------------------------------------------------
" Key Mappings
" -------------------------------------------------------------------------

" Exit insert mode with jj
inoremap jj <Esc>

" Exc to exit terminal mode
tnoremap <Esc> <C-\><C-n>

" ALT+{h,j,k,l} to navigate windows from any mode
tnoremap <A-h> <C-\><C-N><C-w>h
tnoremap <A-j> <C-\><C-N><C-w>j
tnoremap <A-k> <C-\><C-N><C-w>k
tnoremap <A-l> <C-\><C-N><C-w>l
inoremap <A-h> <C-\><C-N><C-w>h
inoremap <A-j> <C-\><C-N><C-w>j
inoremap <A-k> <C-\><C-N><C-w>k
inoremap <A-l> <C-\><C-N><C-w>l
nnoremap <A-h> <C-w>h
nnoremap <A-j> <C-w>j
nnoremap <A-k> <C-w>k
nnoremap <A-l> <C-w>l
