#
# ~.zshrc
#
#
#
#----------------------------------------------------------------------------------
# path
#----------------------------------------------------------------------------------
path+=('~/.config/emacs/bin')
export PATH

#----------------------------------------------------------------------------------
# exports
#----------------------------------------------------------------------------------
export SUDO_EDITOR='nvim'
export VISUAL='emacsclient -ca "emacs"'
export EDITOR='nvim'
export BROWSER='brave'
export TERM='xterm-256color'
export MANPAGER="sh -c 'col -bx | bat -l man -p'" # Use bat for man pager
export QT_STYLE_OVERRIDE=kvantum
#export GDK_DPI_SCALE=1.5
#export QT_SCALE_FACTOR=1.5
#export GDK_SCALE=2

#----------------------------------------------------------------------------------
# tab complete
#----------------------------------------------------------------------------------
autoload -Uz compinit
zstyle ':completion:*' menu select
zmodload zsh/complist
compinit
_comp_options+=(globdots) # include hidden files

#----------------------------------------------------------------------------------
# history
#----------------------------------------------------------------------------------
HISTSIZE=1000
SAVEHIST=1000
HISTFILE=~/.cache/zsh/history

#----------------------------------------------------------------------------------
# options
#----------------------------------------------------------------------------------
setopt autocd # cd just by typing directory name

#----------------------------------------------------------------------------------
# vi mode
#----------------------------------------------------------------------------------
bindkey -v
export KEYTIMEOUT=25 # 25 ms to allow for keychords

#----------------------------------------------------------------------------------
# key bindings
#----------------------------------------------------------------------------------
bindkey -M viins jj vi-cmd-mode # exit insert mode with jj

# use vim keys in tab complete menu:
bindkey -M menuselect 'h' vi-backward-char
bindkey -M menuselect 'k' vi-up-line-or-history
bindkey -M menuselect 'l' vi-forward-char
bindkey -M menuselect 'j' vi-down-line-or-history

# load alias from aliasrc file
[ -f "$HOME/.config/aliasrc" ] && source "$HOME/.config/aliasrc"

#----------------------------------------------------------------------------------
# Ranger CD
#----------------------------------------------------------------------------------
function ranger-cd {
    tempfile="$(mktemp -t tmp.XXXXXX)"
    /usr/bin/ranger --choosedir="$tempfile" "${@:-$(pwd)}"
    test -f "$tempfile" &&
    if [ "$(cat -- "$tempfile")" != "$(echo -n `pwd`)" ]; then
        cd -- "$(cat "$tempfile")"
    fi
    rm -f -- "$tempfile"
}


bindkey -s '^O' 'ranger-cd\n'
#ranger-cd will fire for Ctrl+O

#----------------------------------------------------------------------------------
# zplug
#----------------------------------------------------------------------------------

# https://github.com/zplug/zplug
source /usr/share/zsh/scripts/zplug/init.zsh

# Adds vi mode indicator to spaceship-prompt
# https://github.com/spaceship-prompt/spaceship-vi-mode
zplug "spaceship-prompt/spaceship-vi-mode"

# Install plugins if there are plugins that have not been installed
if ! zplug check --verbose; then
    printf "Install? [y/N]: "
    if read -q; then
        echo; zplug install
    fi
fi

# Then, source plugins and add commands to $PATH
zplug load

#----------------------------------------------------------------------------------
# plugins - should be last
#----------------------------------------------------------------------------------

# syntax highlighting
source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
# autocomplete
source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh
# spaceship prompt
source /usr/lib/spaceship-prompt/spaceship.zsh
