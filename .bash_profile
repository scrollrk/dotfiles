#
# ~/.bash_profile
#

# Start X if no DISPLAY and TTY = 1
# if [[ -z $DISPLAY ]] && [[ $(tty) = /dev/tty1 ]]; then
#   startx
# fi

# start gnome-keyring-daemon
# [ -n "$DESKTOP_SESSION" ] && eval $(gnome-keyring-daemon --start); export SSH_AUTH_SOCK

# Load .bashrc settings
[[ -f ~/.bashrc ]] && . ~/.bashrc

