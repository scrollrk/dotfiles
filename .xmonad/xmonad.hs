------------------------------------------------------------------------
-- Imports
------------------------------------------------------------------------
import GHC.IO.Handle ( Handle )
import Data.List ()
import Data.Monoid ( All )
import System.Exit ()
--import XMonad.Hooks.WindowSwallowing
import XMonad
    ( (|||),
      xmonad,
      doF,
      doIgnore,
      title,
      (=?),
      className,
      (<+>),
      withFocused,
      windows,
      sendMessage,
      kill,
      spawn,
      propModeReplace,
      changeProperty32,
      io,
      getAtom,
      asks,
      withDisplay,
      gets,
      mod4Mask,
      Full(Full),
      Event,
      ManageHook,
      IncMasterN(IncMasterN),
      Resize(Expand, Shrink),
      ChangeLayout(NextLayout),
      XConf(theRoot),
      XConfig(terminal, focusFollowsMouse, borderWidth, modMask,
              workspaces, normalBorderColor, focusedBorderColor, manageHook,
              handleEventHook, logHook, startupHook, layoutHook),
      XState(windowset),
      Dimension,
      X,
      KeyMask, doFloat )
import XMonad.Actions.Warp ( warpToWindow )
import XMonad.Config ( Default(def) )
import XMonad.Hooks.DynamicLog
    ( dynamicLogWithPP,
      shorten,
      wrap,
      xmobarColor,
      xmobarPP,
      PP(ppOutput, ppCurrent, ppVisible, ppHidden, ppTitle, ppUrgent,
         ppLayout, ppExtras, ppSep, ppOrder) )
import XMonad.Hooks.EwmhDesktops ( ewmh, ewmhFullscreen )
import XMonad.Hooks.InsertPosition
    ( insertPosition, Focus(Newer), Position(Below) )
import XMonad.Hooks.ManageDocks
    ( ToggleStruts(ToggleStruts),
      avoidStruts,
      checkDock,
      docks,
      manageDocks )
import XMonad.Hooks.ManageHelpers
    ( (-?>), doFullFloat, doCenterFloat, isDialog )
import XMonad.Hooks.UrgencyHook
    ( withUrgencyHook, NoUrgencyHook(NoUrgencyHook) )
import XMonad.Hooks.SetWMName ( setWMName )
import XMonad.Layout.Grid ( Grid(GridRatio) )
import XMonad.Layout.IfMax ( IfMax(IfMax) )
import XMonad.Layout.Mosaic ()
import XMonad.Layout.MultiColumns ( multiCol )
import XMonad.Layout.NoBorders ( noBorders, smartBorders, hasBorder )
--import XMonad.Layout.OnHost                     -- Look into for per computer layouts
import XMonad.Layout.Renamed ( renamed, Rename(Replace) )
import XMonad.Layout.ResizableTile
    ( MirrorResize(MirrorExpand, MirrorShrink), ResizableTall(ResizableTall) )
import XMonad.Util.EZConfig ( additionalKeysP )
import XMonad.Util.Run ( hPutStrLn, spawnPipe )
import XMonad.Util.SpawnOnce (spawnOnce)

import qualified XMonad.StackSet as W
import XMonad.ManageHook ( composeAll, (-->) )

------------------------------------------------------------------------
-- Variables
------------------------------------------------------------------------
-- Set Mod Mask to Super key
myModMask :: KeyMask
myModMask = mod4Mask

myTerminal :: String
myTerminal = "alacritty"

myBrowser :: String
myBrowser = "brave"

myCliMusicPlayer :: String
myCliMusicPlayer = myTerminal ++ " -t spotify -e ncspot"

myCliEmailClient :: String
myCliEmailClient = myTerminal ++ " -t neomutt -e neomutt"

myGuiEmailClient :: String
myGuiEmailClient = "thunderbird"

myCliFileBrowser :: String
myCliFileBrowser = myTerminal ++ " -t ranger -e ranger"

myGuiFileBrowser :: String
myGuiFileBrowser = "nemo"

myCliProcessManager :: String
myCliProcessManager = myTerminal ++ " -t htop -e htop"

myGuiProcessManager :: String
myGuiProcessManager = "lxtask"

myFocusFollowsMouse :: Bool
myFocusFollowsMouse = True

-- Warp mouse to center of window
bringMouse :: X()
bringMouse = warpToWindow (5/10) (5/10)

myWorkspaces :: [[Char]]
myWorkspaces = ["1","2","3","4","5","6","7","8","9"]

myBorderWidth :: Dimension
myBorderWidth = 3

myNormalBorderColor :: String
myNormalBorderColor  = "#1d1f21"

myFocusedBorderColor :: String
myFocusedBorderColor = "#81a2be"

currentWorkspaceFG :: String
currentWorkspaceFG = "#1d1f21"

currentWorkspaceBG :: String
currentWorkspaceBG = "#f0c674"

visibleWorkspaceFG :: String
visibleWorkspaceFG = "#ffffff"

visibleWorkspaceBG :: String
visibleWorkspaceBG = ""

hiddenWorkspaceFG :: String
hiddenWorkspaceFG = "#969896"

hiddenWorkspaceBG :: String
hiddenWorkspaceBG = ""

windowTitleFG :: String
windowTitleFG = "#b5bd68"

windowTitleBG :: String
windowTitleBG = ""

urgentWorkspaceFG :: String
urgentWorkspaceFG = "#1d1f21"

urgentWorkspaceBG :: String
urgentWorkspaceBG = "#cc6666"

layoutTitleFG :: String
layoutTitleFG = "#ffffff"

layoutTitleBG :: String
layoutTitleBG = ""

windowCount :: X (Maybe String)
windowCount = gets $ Just . show . length . W.integrate' . W.stack . W.workspace . W.current . windowset

------------------------------------------------------------------------
-- EWMH fullscreen patch
------------------------------------------------------------------------
fullscreenFix :: XConfig a -> XConfig a
fullscreenFix c = c {
                      startupHook = startupHook c +++ setSupportedWithFullscreen
                    }
                  where x +++ y = mappend x y

setSupportedWithFullscreen :: X ()
setSupportedWithFullscreen = withDisplay $ \dpy -> do
    r <- asks theRoot
    a <- getAtom "_NET_SUPPORTED"
    c <- getAtom "ATOM"
    supp <- mapM getAtom ["_NET_WM_STATE_HIDDEN"
                         ,"_NET_WM_STATE_FULLSCREEN"
                         ,"_NET_NUMBER_OF_DESKTOPS"
                         ,"_NET_CLIENT_LIST"
                         ,"_NET_CLIENT_LIST_STACKING"
                         ,"_NET_CURRENT_DESKTOP"
                         ,"_NET_DESKTOP_NAMES"
                         ,"_NET_ACTIVE_WINDOW"
                         ,"_NET_WM_DESKTOP"
                         ,"_NET_WM_STRUT"
                         ]
    io $ changeProperty32 dpy r a c propModeReplace (fmap fromIntegral supp)

    setWMName "xmonad"

------------------------------------------------------------------------
-- Startup hook
------------------------------------------------------------------------
myStartupHook :: X ()
myStartupHook = do
  spawnOnce "~/.scripts/autostart.sh &"
  spawnOnce "~/.config/trayer/launch.sh &"
------------------------------------------------------------------------
-- Key bindings
------------------------------------------------------------------------
myKeys :: [(String, X ())]
myKeys =
  [ -- Launcher
    -- dmemu style rofi to use .desktop files
    ("M-d", spawn "$HOME/.config/rofi/launch.sh")
    -- Frequently used programs. For GUI versions add alt.
  , ("M-<Return>", spawn myTerminal)
  , ("M-b", spawn myBrowser)
  , ("M-s", spawn myCliMusicPlayer)
  , ("M-<Tab>", spawn myCliFileBrowser)
  , ("M-M1-<Tab>", spawn myGuiFileBrowser)
  , ("M-<Delete>", spawn myCliProcessManager)
  , ("M-M1-<Delete>", spawn myGuiProcessManager)
  , ("M-e", spawn myGuiEmailClient)
  , ("M-M1-e", spawn myCliEmailClient)
  -- Screenshots
  , ("M-p", spawn "maim -s $HOME/Pictures/$(date +%F_%H:%M:%S)_screenshot.png")
  , ("M-S-p", spawn "maim $HOME/Pictures/$(date +%F_%H:%M:%S)_screenshot.png")
  -- Close window
  , ("M-q", kill)
  -- Rotate through the available layout algorithms
  , ("M-<Space>", sendMessage NextLayout)
  -- Focus Master
  , ("M-m", windows W.focusMaster >> bringMouse)
  -- Make focused window the master
  , ("M-S-m", windows W.swapMaster >> bringMouse)
  -- Window navigation
  , ("M-k", windows W.focusUp >> bringMouse)
  , ("M-j", windows W.focusDown >> bringMouse)
  -- Window swap
  , ("M-S-k", windows W.swapUp >> bringMouse)
  , ("M-S-j", windows W.swapDown >> bringMouse)
  -- Shrink
  , ("M-h", sendMessage Shrink)
  -- Expand
  , ("M-l", sendMessage Expand)
  -- (Stack) Expand Tall Slave Windows
  , ("M-z", sendMessage MirrorShrink)
  -- (Stack) Shrink Tall Slave Windows
  , ("M-a", sendMessage MirrorExpand)
  -- Push window back into tiling
  , ("M-t", withFocused $ windows . W.sink)
  -- Increment the number of windows in the master area
  , ("M-,", sendMessage (IncMasterN 1))
  -- Deincrement the number of windows in the master area
  , ("M-.", sendMessage (IncMasterN (-1)))
    -- Toggle the status bar gap
  , ("M-S-s", sendMessage ToggleStruts)
  -- Quit xmonad
  --, ("M-C-e", io exitSuccess)
  , ("M-S-e", spawn "$HOME/.xmonad/logout.sh")
  --, ("M-S-e", spawn "qdbus org.kde.ksmserver /KSMServer logout 1 0 0")
  -- Restart xmonad
  , ("M-C-r", spawn "xmonad --recompile; xmonad --restart")
  -- Audio Controls
  , ("<XF86AudioRaiseVolume>", spawn "pactl set-sink-volume 0 +5%")
  , ("<XF86AudioLowerVolume>", spawn "pactl set-sink-volume 0 -5%")
  , ("<XF86AudioMute>", spawn "pactl set-sink-mute 0 toggle")
  , ("<XF86AudioPlay>", spawn "playerctl play-pause")
  , ("<XF86AudioPrev>", spawn "playerctl previous")
  , ("<XF86AudioNext>", spawn "playerctl next")
  -- Brightness Controls
  , ("<XF86MonBrightnessUp>", spawn "brightnessctl s +5%")
  , ("<XF86MonBrightnessDown>", spawn "brightnessctl s 5%-")
  , ("<XF86KbdBrightnessUp>", spawn "brightnessctl -d smc::kbd_backlight s +5%")
  , ("<XF86KbdBrightnessDown>", spawn "brightnessctl -d smc::kbd_backlight s 5%-")
  -- Eject Disk
  --, ("<XF86Eject>", spawn "eject -r")
  ]

------------------------------------------------------------------------
-- Window rules:
------------------------------------------------------------------------
myManageHook :: ManageHook
myManageHook =
  manageDocks <+>
  composeAll
  [ isDialog                                        --> doFloat
  , className =? "confirm"                          --> doCenterFloat
  , className =? "file_progress"                    --> doCenterFloat
  , className =? "dialog"                           --> doCenterFloat
  , className =? "download"                         --> doCenterFloat
  , className =? "error"                            --> doCenterFloat
  , className =? "notification"                     --> doCenterFloat
  , className =? "splash"                           --> doCenterFloat <+> hasBorder False
  , className =? "Xmessage"                         --> doCenterFloat
--  , title =? "Steam"                                --> doFloat <+> hasBorder False
  , title =? "Steam Guard - Computer Authorization Required" --> doFloat <+> hasBorder False
  ]

------------------------------------------------------------------------
-- Event handling
------------------------------------------------------------------------
--myEventHook :: Event -> X All
--myEventHook = fullscreenEventHook
--          <+> docksEventHook
--          <+> swallowEventHook (className =? "Alacritty" <||> className =? "Termite") (return True)

------------------------------------------------------------------------
-- Status bars and logging
------------------------------------------------------------------------
myLogHook :: Handle -> X()
myLogHook h = dynamicLogWithPP xmobarPP
  { ppOutput  = hPutStrLn h
  , ppCurrent = xmobarColor currentWorkspaceFG currentWorkspaceBG . wrap " " " "
  , ppVisible = xmobarColor visibleWorkspaceFG visibleWorkspaceBG . wrap " " " "
  , ppHidden  = xmobarColor hiddenWorkspaceFG  hiddenWorkspaceBG  . wrap " " " "
  , ppTitle   = xmobarColor windowTitleFG      windowTitleBG      . wrap "" " "  . shorten 50
  , ppUrgent  = xmobarColor urgentWorkspaceFG  urgentWorkspaceBG  . wrap " " " "
  , ppLayout  = xmobarColor layoutTitleFG      layoutTitleBG      . wrap " " " "
  , ppExtras  = [windowCount]
  , ppSep     = ""
  , ppOrder   = \(ws:l:t:ex) -> [ws,l] ++ [t]
  }

------------------------------------------------------------------------
-- Now run xmonad with all the defaults we set up.
------------------------------------------------------------------------
main :: IO()
main = do
  h <- spawnPipe "xmobar $HOME/.config/xmobar/.xmobarrc0"
  xmonad
    $ withUrgencyHook NoUrgencyHook
    $ fullscreenFix
    $ ewmhFullscreen
    $ ewmh
    $ docks
    $ def
      { terminal           = myTerminal
      , focusFollowsMouse  = myFocusFollowsMouse
      , borderWidth        = myBorderWidth
      , modMask            = myModMask
      , workspaces         = myWorkspaces
      , normalBorderColor  = myNormalBorderColor
      , focusedBorderColor = myFocusedBorderColor
      , manageHook         = myManageHook
--      , manageHook         = manageHook kdeConfig <+> myManageHook
--      , handleEventHook    = myEventHook
--      , logHook            = logHook kdeConfig -- <+> myLogHook h
      , logHook            = myLogHook h
      , startupHook        = myStartupHook
      , layoutHook         = avoidStruts
                           $ sTall ||| mCol ||| grid ||| full
      } `additionalKeysP` myKeys
  where
------------------------------------------------------------------------
-- Layouts:
------------------------------------------------------------------------
    full        = renamed [Replace "[M]"] -- Full
                $ noBorders
                Full

    grid        = renamed [Replace "[+]"] -- Grid Ratio 1
                $ smartBorders
                $ GridRatio 2

    _grid       = renamed [Replace "[+]"] -- Grid Ratio 3/2
                $ smartBorders
                $ GridRatio (3/2)

    mCol        = renamed [Replace "||"] -- Multi columns
                $ smartBorders
                $ multiCol [nmaster] 1 delta (-0.5)

    tall        = renamed [Replace "[]="] -- Tall
                $ smartBorders
                $ ResizableTall nmaster delta ratio []

    sTall       = renamed [Replace "[S]="] (IfMax 3 tall _grid) -- Smart Tall

    -- The default number of windows in the master pane
    nmaster :: Int
    nmaster = 1
    -- Default proportion of screen occupied by master pane
    ratio :: Rational
    ratio = 2/3
    -- Percent of screen to increment by when resizing panes
    delta :: Rational
    delta   = 3/100
