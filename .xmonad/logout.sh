#!/usr/bin/env bash
# Logout.sh
# a dmenu exit script for tiling window managers
# based on oblogout-blurlock and systemd/openrc-compatible
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

DMENURC="$HOME/.config/dmenu/.dmenurc"

# source .dmenurc
if [ -f $DMENURC ]; then
  . $HOME/.config/dmenu/.dmenurc
elif ! [ -f $DMENURC ] && [ -f "/usr/share/dmenu/dmenurc" ]; then
  cp /usr/share/dmenu/dmenurc $HOME/.config/dmenu/.dmenurc
  . $HOME/.config/dmenu/.dmenurc
fi

# define lock
function lock {
  cinnamon-screensaver-command -l
}

# define switch
function switch {
  lock
  if pgrep gdm >> /dev/null; then
    gdmflexiserver
  else
    dm-tool switch-to-greeter
  fi
}

# define logout
function logout {
  if [ $DESKTOP_SESSION = "fluxbox" ];then
    killall fluxbox
  elif [ $DESKTOP_SESSION = "openbox" ]; then
    openbox --exit
  elif [ $DESKTOP_SESSION = "i3" ]; then
    i3-msg exit
  elif [ $DESKTOP_SESSION = "bspwm" ]; then
    bspc quit 1
  elif [ $DESKTOP_SESSION = "xmonad" ]; then
    pkill xmonad
  else
    pkill -KILL -u $USER
  fi
}

# define suspend
function suspend {
  if [ $(cat /proc/1/comm) = "systemd" ]; then
    systemctl suspend
  else
    dbus-send --system --dest=org.freedesktop.ConsoleKit --type=method_call --print-reply --reply-timeout=2000 /org/freedesktop/ConsoleKit/Manager org.freedesktop.ConsoleKit.Manager.Suspend boolean:true
  fi
}

# define hibernate
function hibernate {
  if [ $(cat /proc/1/comm) = "systemd" ]; then
    systemctl hibernate
  else
    dbus-send --system --dest=org.freedesktop.ConsoleKit --type=method_call --print-reply --reply-timeout=2000 /org/freedesktop/ConsoleKit/Manager org.freedesktop.ConsoleKit.Manager.Hibernate boolean:true
  fi
}

# define reboot
function reboot {
  if [ $(cat /proc/1/comm) = "systemd" ]; then
    systemctl reboot
  else
    dbus-send --system --dest=org.freedesktop.ConsoleKit --type=method_call --print-reply --reply-timeout=2000 --dest=org.freedesktop.ConsoleKit /org/freedesktop/ConsoleKit/Manager org.freedesktop.ConsoleKit.Manager.Restart
  fi
}

# define shutdown
function shutdown {
  if [ $(cat /proc/1/comm) = "systemd" ]; then
    systemctl poweroff
  else
    dbus-send --system --dest=org.freedesktop.ConsoleKit --type=method_call --print-reply --reply-timeout=2000 --dest=org.freedesktop.ConsoleKit /org/freedesktop/ConsoleKit/Manager org.freedesktop.ConsoleKit.Manager.Stop
  fi
}

# options menu
if [ -f $DMENURC ]; then
  dmenu_cmd="dmenu $DMENU_OPTIONS"
else
  dmenu_cmd="dmenu"
fi
options=$(echo -e "lock\nlogout\nswitch\nsuspend\nhibernate\nreboot\nshutdown" | $dmenu_cmd -p action:)
case "$options" in
  lock) lock & ;;
  logout) logout & ;;
  switch) switch & ;;
  suspend) suspend & ;;
  hibernate) hibernate & ;;
  reboot) reboot & ;;
  shutdown) shutdown & ;;
esac

exit 0
