#
# ~/.config/fish/config.fish
#

### exports ###

set fish_greeting                         # Supresses fish's intro message
set -Ux EDITOR "nvim"
set -Ux VISUAL "emacs"
set -Ux BROWSER "brave"
set -Ux TERM "xterm-256color"
set -Ux MANPAGER "sh -c 'col -bx | bat -l man -p'" # Use bat for man pager

### path ###

fish_add_path $HOME/.cabal/bin
fish_add_path $HOME/.emacs.d/bin
fish_add_path $HOME/.local/bin

### secrets ###

# enable gnome keyring in terminal
if test -n "$DESKTOP_SESSION"
    set -x (gnome-keyring-daemon --start | string split "=")
end

# load alias from aliasrc file
[ -f "$HOME/.config/aliasrc" ] && source "$HOME/.config/aliasrc"

### Key Bindings ###

# Use vi mode
fish_vi_key_bindings
