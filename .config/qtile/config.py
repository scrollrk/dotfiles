from typing import List  # noqa: F401

from libqtile import bar, layout, widget, hook
from libqtile.config import Click, Drag, Group, Key, Match, Screen
from libqtile.lazy import lazy
from libqtile.utils import guess_terminal

import os
import subprocess

mod = "mod4" # Super
alt = "mod1"
terminal = "alacritty"
browser = "brave"
email = "thunderbird"
cli_file_browser = terminal+" -t ranger -e ranger"
gui_file_browser = "nemo"
rofi = "rofi -modi 'drun' -show drun -theme dmenu.rasi -no-lazy-grab -matching fuzzy"
logout = "./.scripts/logout.sh"
grey = "#969896"
black = "#1d1f21"
green = "#b5bd68"
orange = "#de935f"
red = "#cc6666"
blue = "#81a2be"
yellow = "#f0c674"


keys = [
    # A list of available commands that can be bound to keys can be found
    # at https://docs.qtile.org/en/latest/manual/config/lazy.html
    # Switch between windows
    Key([mod], "h", lazy.layout.left(), desc="Move focus to left"),
    Key([mod], "l", lazy.layout.right(), desc="Move focus to right"),
    Key([mod], "j", lazy.layout.down(), desc="Move focus down"),
    Key([mod], "k", lazy.layout.up(), desc="Move focus up"),
    # Move windows between left/right columns or move up/down in current stack.
    # Moving out of range in Columns layout will create new column.
    Key([mod, "shift"], "h", lazy.layout.shuffle_left(), desc="Move window to the left"),
    Key([mod, "shift"], "l", lazy.layout.shuffle_right(), desc="Move window to the right"),
    Key([mod, "shift"], "j", lazy.layout.shuffle_down(), desc="Move window down"),
    Key([mod, "shift"], "k", lazy.layout.shuffle_up(), desc="Move window up"),
    Key([mod, "shift"], "m", lazy.layout.swap_main(), desc="Swap with main window"),
    Key([alt, "shift"], "h", lazy.layout.swap_column_left(), desc="Swap column left"),
    Key([alt, "shift"], "l", lazy.layout.swap_column_right(), desc="Swap column right"),
    # Grow windows. If current window is on the edge of screen and direction
    # will be to screen edge - window would shrink.
    Key([mod, "control"], "h", lazy.layout.grow_left(), desc="Grow window to the left"),
    Key([mod, "control"], "l", lazy.layout.grow_right(), desc="Grow window to the right"),
    Key([mod, "control"], "j", lazy.layout.grow_down(), desc="Grow window down"),
    Key([mod, "control"], "k", lazy.layout.grow_up(), desc="Grow window up"),
    #Key([mod, "control"], "i", lazy.layout.grow(), desc="Grow window"),
    #Key([mod, "control"], "m", lazy.layout.shrink(), desc="Shrink window"),
    Key([mod, "control"], "o", lazy.layout.maximize(), desc="Toggle window between its minimum and maximum sizes"),
    Key([mod], "n", lazy.layout.normalize(), desc="Reset all window sizes"),
    # Toggle between split and unsplit sides of stack.
    # Split = all windows displayed
    # Unsplit = 1 window displayed, like Max layout, but still with
    # multiple stack panes
    Key([alt], "space", lazy.layout.toggle_split(), desc="Toggle between split and unsplit sides of stack"),
    # Frequently used programs
    Key([mod], "d", lazy.spawn(rofi), desc="Launch a rofi run prompt"),
    Key([mod], "b", lazy.spawn(browser), desc="Launch web browser"),
    Key([mod], "r", lazy.spawncmd(), desc="Spawn a command using a prompt widget"),
    Key([mod], "Tab", lazy.spawn(cli_file_browser), desc="Launch CLI file browser"),
    Key([mod, "control"], "Tab", lazy.spawn(gui_file_browser), desc="Launch GUI file browser"),
    Key([mod], "e", lazy.spawn(email), desc="Launch email"),
    Key([mod], "i", lazy.spawn("emacsclient -ca 'emacs'"), desc="Launch Emacs Client"),
    Key([mod], "Return", lazy.spawn(terminal), desc="Launch terminal"),
    # Key([mod, "control"], "e", lazy.shutdown(), desc="Shutdown Qtile"),
    Key([mod, "shift"], "e", lazy.spawn(logout), desc="Launch exit menu"),
    # Toggle between different layouts as defined below
    Key([mod], "space", lazy.next_layout(), desc="Toggle between layouts"),
    Key([mod, "shift"], "space", lazy.layout.flip(), desc="Mirror layout across the vertical"),
    Key([mod], "t", lazy.window.toggle_floating(), desc="Toggle window to/from floating layout"),
    Key([mod, "control"], "r", lazy.reload_config(), desc="Reload the config"),
    Key([mod], "q", lazy.window.kill(), desc="Kill focused window"),
    Key([], "XF86AudioRaiseVolume", lazy.spawn("pactl set-sink-volume 0 +5%")),
    Key([], "XF86AudioLowerVolume", lazy.spawn("pactl set-sink-volume 0 -5%")),
    Key([], "XF86AudioMute", lazy.spawn("pactl set-sink-mute 0 toggle")),
]

# Autostart
@hook.subscribe.startup_once
def autostart():
    home = os.path.expanduser('~/.scripts/autostart.sh')
    subprocess.run([home])

groups = [Group(i) for i in "12345678"]

for i in groups:
    keys.extend(
        [
            # mod1 + letter of group = switch to group
            Key([mod], i.name, lazy.group[i.name].toscreen(),
                desc="Switch to group {}".format(i.name),),
            # alt + shift + letter of group = switch to & move focused window to group
            Key([alt, "shift"], i.name, lazy.window.togroup(i.name, switch_group=True),
                desc="Switch to & move focused window to group {}".format(i.name),),
            # Or, use below if you prefer not to switch to that group.
            # # mod1 + shift + letter of group = move focused window to group
            Key([mod, "shift"], i.name, lazy.window.togroup(i.name),
                 desc="move focused window to group {}".format(i.name)),
        ]
    )

layout_defaults = {
    'border_width': 5,
    'border_focus': red,
    'border_normal': ['#000000'],
}

layouts = [
    # Try more layouts by unleashing below layouts.
    #layout.Max(),
    # layout.Bsp(),
    # layout.Matrix(),
    # layout.RatioTile(**layout_defaults, ),
    layout.MonadTall(**layout_defaults, ratio = 1/2, single_border_width = 0),
    layout.Columns(**layout_defaults, border_focus_stack=[blue], border_normal_stack = ['#000000'], num_columns = 3, border_on_single = False),
    # layout.MonadWide(),
    # layout.Stack(**layout_defaults, num_stacks=2),
    # layout.Tile(**layout_defaults, ),
    # layout.TreeTab(**layout_defaults, ),
    # layout.Slice(**layout_defaults, )
    # layout.VerticalTile(),
    # layout.Zoomy(**layout_defaults, ),
]

widget_defaults = dict(
    font="Ubuntu Mono Nerd Font",
    fontsize=18,
    padding=10,
    background = black,
)
extension_defaults = widget_defaults.copy()


screens = [
    Screen(
        top=bar.Bar(
            [
                widget.GroupBox(
                    hide_unused = True,
                    padding_y = 6,
                    padding_x = 6,
                    highlight_method = "block",
                    active = grey,
                    block_highlight_text_color = black,
                    this_current_screen_border = yellow,
                ),
                widget.WindowName(
                    foreground = blue,
                ),
                widget.CurrentLayout(
                    foreground = green,
                ),
                widget.Systray(
                    icon_size = 22,
                    padding = 0,
                ),
                widget.Clock(
                    padding = 12,
                    foreground = grey,
                    format="%a %Y-%m-%d %H%M",
                ),
            ],
            23,
            # border_width=[2, 0, 2, 0],  # Draw top and bottom borders
            # border_color=["ff00ff", "000000", "ff00ff", "000000"]  # Borders are magenta
        ),
    ),
]

# Drag floating layouts.
mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(), start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(), start=lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front()),
]

dgroups_key_binder = None
dgroups_app_rules = []  # type: List
follow_mouse_focus = True
bring_front_click = False
cursor_warp = True
floating_layout = layout.Floating(
    ** layout_defaults,
    float_rules=[
        # Run the utility of `xprop` to see the wm class and name of an X client.
        *layout.Floating.default_float_rules,
        Match(wm_class="confirmreset"),  # gitk
        Match(wm_class="makebranch"),  # gitk
        Match(wm_class="maketag"),  # gitk
        Match(wm_class="Conky"),  # conky
        Match(wm_class="pavucontrol"),  # volume control
        Match(wm_class="blueman-manager"),  # bluetooth
        Match(wm_class="meteo-qt"),  # weather
        Match(wm_class="nm-connection-editor"),  # network manager
        Match(wm_class="ssh-askpass"),  # ssh-askpass
        Match(title="branchdialog"),  # gitk
        Match(title="pinentry"),  # GPG key password entry
    ]
)
auto_fullscreen = True
focus_on_window_activation = "smart"
reconfigure_screens = True

# If things like steam games want to auto-minimize themselves when losing
# focus, should we respect this or not?
auto_minimize = True

# XXX: Gasp! We're lying here. In fact, nobody really uses or cares about this
# string besides java UI toolkits; you can see several discussions on the
# mailing lists, GitHub issues, and other WM documentation that suggest setting
# this string if your java app doesn't work correctly. We may as well just lie
# and say that we're a working one by default.
#
# We choose LG3D to maximize irony: it is a 3D non-reparenting WM written in
# java that happens to be on java's whitelist.
wmname = "LG3D"
