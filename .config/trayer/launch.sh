#!/bin/bash
#
# ~/.config/trayer/launch.sh
#
# trayer startup script. Ensures only one trayer is running.

# Terminate any running trayer instances
killall trayer;

# wait until all process have terminated
while pgrep -u $UID -x trayer;
  do sleep 1;
done

# check current resolution and enable HI DPI mode
if [ "$(xrandr | grep 'current 2256 x 1504')" ]; then
  echo HI DPI mode set
  height=33
else
  height=22
fi

# start trayer
trayer --edge top --align right --widthtype request --expand true \
  --SetDockType true --SetPartialStrut true --height $height --padding 15 \
  --tint 0x1E1E1E --alpha 0 --transparent true &
