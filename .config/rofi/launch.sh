#!/bin/bash
#
# ~/.config/rofi/launcher.sh
#
# trayer startup script. Ensures only one trayer is running.
# requires xdpyinfo

# check current resolution and enable HI DPI mode
if [ "$(xrandr | grep 'current 2256 x 1504')" ]; then
  echo HI DPI mode set
  theme=dmenu_HighDPI.rasi
else
  theme=dmenu.rasi
fi

# start rofi
rofi -modi "drun" -show drun -theme $theme -no-lazy-grab -matching fuzzy
