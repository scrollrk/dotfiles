#!/bin/sh
# Programs to start on window manager first run

# Session Manager
lxsession --de="LG3D" --session="xmonad" --noautostart &

# Email encryption bridge
protonmail-bridge &

# Compositor
picom -b &

# Notifications
dunst &

# Tray icons
nm-applet &
blueman-applet &
#steam &
flameshot &
dropbox &
#redshift-gtk &
#xset m 20/10 10 r rate 500 30 b on &
#systemctl --user enable --now gamemoded &
pasystray &

# Start Emacs daemon to speed up opening emacs
emacs --daemon &

# Background
$HOME/.fehbg &

function session_id() {
  # Gets Session ID for current user
  loginctl list-sessions --no-legend | grep "$(whoami)" | awk '{ print $1 }'
}

# Screensaver
export PRIMARY_DISPLAY="$(xrandr | awk '/ primary/{print $1}')"
export SESSION_ID=$(session_id)

xidlehook \
  `# Don't lock when there's a fullscreen application` \
  --not-when-fullscreen \
  `# Don't lock when there's audio playing` \
  --not-when-audio \
  `# Dim the screen after 9 min, undim if user becomes active` \
  --timer 540 \
    'xrandr --output "$PRIMARY_DISPLAY" --brightness .1' \
    'xrandr --output "$PRIMARY_DISPLAY" --brightness 1' \
  `# Undim and lock after 1 more minute` \
  --timer 60 \
    'xrandr --output "$PRIMARY_DISPLAY" --brightness 1; \
    if [ $(loginctl show-session -p Active "$SESSION_ID" | grep yes) ]; \
      then gdmflexiserver; \
    fi' \
  `# Finally, suspend 50 minutes after locking` \
  --timer 3000 \
    'systemctl hibernate' \
    '' &
