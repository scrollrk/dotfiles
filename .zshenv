typeset -U PATH path
path=("$HOME/.local/bin" "$HOME/.emacs.d/bin" "$path[@]")
export PATH

# Moved to ~.config/systemd/user/gnome-keyring.service
# enable gnome keyring in terminal
#if [ -n "$DESKTOP_SESSION" ]; then
#  eval $(gnome-keyring-daemon --start)
#  export SSH_AUTH_SOCK
#fi
# New version? with 2 env variables
#if [ -n "$DESKTOP_SESSION" ];then
#    for env_var in $(gnome-keyring-daemon --start); do
#        export env_var
#    done
#fi
